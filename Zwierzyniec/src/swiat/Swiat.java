package swiat;

import java.awt.*;
import javax.swing.JFrame;
import organizm.Organizm;
import rosliny.Ciern;
import rosliny.Jagody;
import rosliny.Trawa;
import zwierzeta.Antylopa;
import zwierzeta.Czlowiek;
import zwierzeta.Lew;
import zwierzeta.Owca;
import zwierzeta.Wilk;

public class Swiat extends JFrame{
    public Organizm[][] organizmy=new Organizm[20][20];
    public int zwierzeta=0;
    public int rosliny=0;
    public int najstarszyOrganizm=0;
    public int populacja=0;

    public Swiat(){
        for(int i=0; i<20; i++){
            for(int j=0; j<20; j++){
                organizmy[i][j]=null;
            }
        }
        DodajZwierzeta();
        DodajRosliny();
        populacja+=16;
    }
    private void DodajRosliny(){
        int x=0;
        int y=0;
        Organizm trawa=new Trawa(this);
        x=trawa.ZwrocPolozenieX();
        y=trawa.ZwrocPolozenieY();
        organizmy[x][y]=trawa;
        Organizm trawa2=new Trawa(this);
        x=trawa2.ZwrocPolozenieX();
        y=trawa2.ZwrocPolozenieY();
        organizmy[x][y]=trawa2;

        Organizm ciern=new Ciern(this);
        x=ciern.ZwrocPolozenieX();
        y=ciern.ZwrocPolozenieY();
        organizmy[x][y]=ciern;
        Organizm ciern2=new Ciern(this);
        x=ciern2.ZwrocPolozenieX();
        y=ciern2.ZwrocPolozenieY();
        organizmy[x][y]=ciern2;

        Organizm jagoda=new Jagody(this);
        x=jagoda.ZwrocPolozenieX();
        y=jagoda.ZwrocPolozenieY();
        organizmy[x][y]=jagoda;
        Organizm jagoda2=new Jagody(this);
        x=jagoda2.ZwrocPolozenieX();
        y=jagoda2.ZwrocPolozenieY();
        organizmy[x][y]=jagoda2;
        rosliny+=6;
    }
    private void DodajZwierzeta(){
        int x=0;
        int y=0;

        Organizm wilk=new Wilk(this);
        x=wilk.ZwrocPolozenieX();
        y=wilk.ZwrocPolozenieY();
        organizmy[x][y]=wilk;
        Organizm wilk2=new Wilk(this);
        x=wilk2.ZwrocPolozenieX();
        y=wilk2.ZwrocPolozenieY();
        organizmy[x][y]=wilk2;

        Organizm owca=new Owca(this);
        x=owca.ZwrocPolozenieX();
        y=owca.ZwrocPolozenieY();
        organizmy[x][y]=owca;
        Organizm owca2=new Owca(this);
        x=owca2.ZwrocPolozenieX();
        y=owca2.ZwrocPolozenieY();
        organizmy[x][y]=owca2;

        Organizm antylopa=new Antylopa(this);
        x=antylopa.ZwrocPolozenieX();
        y=antylopa.ZwrocPolozenieY();
        organizmy[x][y]=antylopa;
        Organizm antylopa2=new Antylopa(this);
        x=antylopa2.ZwrocPolozenieX();
        y=antylopa2.ZwrocPolozenieY();
        organizmy[x][y]=antylopa2;

        Organizm lew=new Lew(this);
        x=lew.ZwrocPolozenieX();
        y=lew.ZwrocPolozenieY();
        organizmy[x][y]=lew;
        Organizm lew2=new Lew(this);
        x=lew2.ZwrocPolozenieX();
        y=lew2.ZwrocPolozenieY();
        organizmy[x][y]=lew2;

        Organizm czlowiek=new Czlowiek(this);
        x=czlowiek.ZwrocPolozenieX();
        y=czlowiek.ZwrocPolozenieY();
        organizmy[x][y]=czlowiek;
        Organizm czlowiek2=new Czlowiek(this);
        x=czlowiek2.ZwrocPolozenieX();
        y=czlowiek2.ZwrocPolozenieY();
        organizmy[x][y]=czlowiek2;
        zwierzeta+=10;
    }
    public void RysujSwiat(Graphics2D g2d){
        for(int i=0; i<20; i++){
            for(int j=0; j<20; j++){
                if(organizmy[i][j]!=null){
                    if(organizmy[i][j].ZwrocNazwe()=='W'){
                        g2d.setColor(Color.DARK_GRAY);
                    }
                    else if(organizmy[i][j].ZwrocNazwe()=='O'){
                        g2d.setColor(Color.WHITE);
                    }
                    else if(organizmy[i][j].ZwrocNazwe()=='L'){
                        g2d.setColor(new Color(255,135,20));
                    }
                    else if(organizmy[i][j].ZwrocNazwe()=='P'){
                        g2d.setColor(Color.BLACK);
                    }
                    else if(organizmy[i][j].ZwrocNazwe()=='A'){
                        g2d.setColor(Color.YELLOW);
                    }
                    else if(organizmy[i][j].ZwrocNazwe()=='T'){
                        g2d.setColor(new Color(124,252,0));
                    }
                    else if(organizmy[i][j].ZwrocNazwe()=='C'){
                        g2d.setColor(new Color(150,75,0));
                    }
                    else if(organizmy[i][j].ZwrocNazwe()=='J'){
                        g2d.setColor(new Color(102,0,102));
                    }
                    g2d.fillRect((i*20)+3, (j*20)+3, 10, 10);
                    g2d.drawRect((i*20)+3, (j*20)+3, 10, 10);
                }
                else{
                    g2d.setColor(Color.LIGHT_GRAY);
                    g2d.fillRect((i*20)+3, (j*20)+3, 10, 10);
                    g2d.drawRect((i*20)+3, (j*20)+3, 10, 10);
                }
            }
        }
    }
    public void WykonajTure(){
        for(int i=0; i<20; i++){
            for(int j=0; j<20; j++){
                if(organizmy[i][j]!=null){
                    organizmy[i][j].ZmienRuch(1);
                }
            }
        }

        for(int k=8;k>=0;k--){
            for(int l=najstarszyOrganizm; l>=0; l--){
                for(int i=0; i<20; i++){
                    for(int j=0; j<20; j++){
                        if(organizmy[i][j]!=null && organizmy[i][j].ZwrocRuch()==1 && organizmy[i][j].ZwrocInicjatywe()==k && organizmy[i][j].ZwrocWiek()==l){
                            Organizm tmp=organizmy[i][j];
                            int wiek=tmp.ZwrocWiek();
                            wiek++;
                            tmp.ZmienWiek(wiek);
                            if(wiek>najstarszyOrganizm){
                                najstarszyOrganizm=wiek;
                            }
                            if(tmp.ZwrocRodzenie()>0){
                                int rodzenie=tmp.ZwrocRodzenie();
                                rodzenie--;
                                tmp.ZmienRodzenie(rodzenie);
                            }
                            tmp.Akcja(tmp,tmp.ZwrocNazwe());
                            tmp.ZmienRuch(0);
                        }
                    }
                }
            }
        }
    }
}
