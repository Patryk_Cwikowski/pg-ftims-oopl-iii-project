package zwierzeta;

import java.util.Random;
import organizm.Organizm;
import swiat.Swiat;
import zwierze.Zwierze;

public class Antylopa extends Zwierze{
    public Antylopa(Swiat swO){
        Random generator = new Random();
        nazwa='A';
        sila=4;
        inicjatywa=4;
        polozenie_x=generator.nextInt(19) + 1;
        polozenie_y=generator.nextInt(19) + 1;
        ruch=0;
        rodzenie=0;
        wiek=0;
        sw=swO;
    }
    @Override
    public void Akcja(Organizm tmp, char nazwa){
        Swiat sw=tmp.ZwrocSwiat();
        Random generator = new Random();
        int wyznacz=1;
        while(wyznacz==1){
            int liczba=generator.nextInt(4) + 1;
            int polozenie=0;
            int polozenieX=0;
            int polozenieY=0;
            switch(liczba){
                case 1:
                    polozenieX-=2;
                    break;
                case 2:
                    polozenieX=2;
                    break;
                case 3:
                    polozenieY-=2;
                    break;
                case 4:
                    polozenieY=2;
                    break;
            }
            if(polozenieX!=0){
                polozenie=tmp.ZwrocPolozenieX();
                polozenie=polozenie+polozenieX;
                if(polozenie>=20 || polozenie<0){
                    polozenie=polozenie-polozenieX-polozenieX;
                }
                if(sw.organizmy[polozenie][tmp.ZwrocPolozenieY()]!=null){
                    tmp.Kolizja(tmp,sw.organizmy[polozenie][tmp.ZwrocPolozenieY()]);
                    wyznacz=0;

                }
                else{
                    int x=tmp.ZwrocPolozenieX();
                    int y=tmp.ZwrocPolozenieY();
                    tmp.ZmienPolozenieX(polozenie);
                    sw.organizmy[x][y]=null;
                    sw.organizmy[polozenie][y]=tmp;
                    wyznacz=0;
                }
            }
            else if(polozenieY!=0){
                polozenie=tmp.ZwrocPolozenieY();
                polozenie=polozenie+polozenieY;
                if(polozenie>=20 || polozenie<0){
                    polozenie=polozenie-polozenieY-polozenieY;
                }
                if(sw.organizmy[tmp.ZwrocPolozenieX()][polozenie]!=null){
                    tmp.Kolizja(tmp, sw.organizmy[tmp.ZwrocPolozenieX()][polozenie]);
                    wyznacz=0;

                }
                else{
                    int x=tmp.ZwrocPolozenieX();
                    int y=tmp.ZwrocPolozenieY();
                    tmp.ZmienPolozenieY(polozenie);
                    sw.organizmy[x][y]=null;
                    sw.organizmy[x][polozenie]=tmp;
                    wyznacz=0;
                }
            }
        }
    }
    @Override
    public void Kolizja(Organizm tmp1, Organizm tmp2){
        Swiat sw=tmp1.ZwrocSwiat();
        Organizm nowy=null;
        int polozenieX1=tmp1.ZwrocPolozenieX();
        int polozenieY1=tmp1.ZwrocPolozenieY();
        if(tmp1.ZwrocNazwe()==tmp2.ZwrocNazwe()){
            int rodzenie1=tmp1.ZwrocRodzenie();
            int rodzenie2=tmp2.ZwrocRodzenie();
            if(rodzenie1==0 && rodzenie2==0){
                int rodzenie=5;
                tmp1.ZmienRodzenie(rodzenie);
                tmp2.ZmienRodzenie(rodzenie);
                int polozenieX=polozenieX1+1;
                int polozenieY=polozenieY1;
                super.UstalPolozenie(polozenieX1, polozenieY1, polozenieX, polozenieY);
                nowy=new Antylopa(sw);

                super.DodajZwierze(nowy,tmp2,polozenieX,polozenieY);
            }
        }
        else{
            super.Kolizja(tmp1,tmp2);
        }
    }
}