package zwierzeta;

import java.util.Random;
import organizm.Organizm;
import swiat.Swiat;
import zwierze.Zwierze;

public class Lew extends Zwierze{
    public Lew(Swiat swO){
        Random generator = new Random();
        nazwa='L';
        sila=11;
        inicjatywa=7;
        polozenie_x=generator.nextInt(19) + 1;
        polozenie_y=generator.nextInt(19) + 1;
        ruch=0;
        rodzenie=0;
        wiek=0;
        sw=swO;
    }
    @Override
    public void Kolizja(Organizm tmp1, Organizm tmp2){
        Swiat sw=tmp1.ZwrocSwiat();
        Organizm nowy=null;
        int polozenieX1=tmp1.ZwrocPolozenieX();
        int polozenieY1=tmp1.ZwrocPolozenieY();
        if(tmp1.ZwrocNazwe()==tmp2.ZwrocNazwe()){
            int rodzenie1=tmp1.ZwrocRodzenie();
            int rodzenie2=tmp2.ZwrocRodzenie();
            if(rodzenie1==0 && rodzenie2==0){
                int rodzenie=5;
                tmp1.ZmienRodzenie(rodzenie);
                tmp2.ZmienRodzenie(rodzenie);
                int polozenieX=polozenieX1+1;
                int polozenieY=polozenieY1;
                super.UstalPolozenie(polozenieX1, polozenieY1, polozenieX, polozenieY);
                nowy=new Lew(sw);

                super.DodajZwierze(nowy,tmp2,polozenieX,polozenieY);
            }
        }
        else{
            super.Kolizja(tmp1,tmp2);
        }
    }
}