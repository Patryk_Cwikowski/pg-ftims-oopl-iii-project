package zwierzyniec;

import javax.swing.JFrame;
import javax.swing.JLabel;


public class Wyjatek extends JFrame{
    public Wyjatek(String akcja) {
        super("UWAGA!!!");
        JLabel tekst=new JLabel();
        if("odczyt".equals(akcja)){
            tekst.setText("                 Brak pliku do odczytu!");
        }
        else if("zapis".equals(akcja)){
            tekst.setText("                 Nie mozna utworzyc pliku!");
        }
        add(tekst);
        setSize(250, 170);
        setLocation(350,200);
        setVisible(true);
    }
}
