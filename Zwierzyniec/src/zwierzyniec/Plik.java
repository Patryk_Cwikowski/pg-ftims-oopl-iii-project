package zwierzyniec;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import organizm.Organizm;
import rosliny.Ciern;
import rosliny.Jagody;
import rosliny.Trawa;
import swiat.Swiat;
import zwierzeta.Antylopa;
import zwierzeta.Czlowiek;
import zwierzeta.Lew;
import zwierzeta.Owca;
import zwierzeta.Wilk;

public class Plik {
    public Plik(){}
    public void PlikZapisz(Organizm organizmy[][]) throws FileNotFoundException{
        PrintWriter zapis = new PrintWriter("swiat.txt");
        for(int i=0; i<20; i++){
            for(int j=0; j<20; j++){
                if(organizmy[i][j]!=null){
                    zapis.println(organizmy[i][j].ZwrocNazwe());
                }
                else{
                    zapis.println("x");
                }
            }
        }
        zapis.close();
    }
    public void PlikOdczytaj(Swiat sw) throws FileNotFoundException{
        File file = new File("swiat.txt");
        Scanner in = new Scanner(file);
        for(int i=0; i<20; i++){
            for(int j=0; j<20; j++){
                sw.organizmy[i][j]=null;
            }
        }
        sw.populacja=0;
        sw.rosliny=0;
        sw.zwierzeta=0;
        for(int i=0; i<20; i++){
            for(int j=0; j<20; j++){
                Organizm nowy=null;
                String organizm = in.nextLine();
                switch (organizm) {
                    case "L":
                        nowy = new Lew(sw);
                        break;
                    case "W":
                        nowy=new Wilk(sw);
                        break;
                    case "A":
                        nowy=new Antylopa(sw);
                        break;
                    case "P":
                        nowy=new Czlowiek(sw);
                        break;
                    case "O":
                        nowy=new Owca(sw);
                        break;
                    case "T":
                        nowy=new Trawa(sw);
                        break;
                    case "J":
                        nowy=new Jagody(sw);
                        break;
                    case "C":
                        nowy=new Ciern(sw);
                        break;
                }
                if(nowy!=null){
                    char nazwa=nowy.ZwrocNazwe();
                    if(nazwa=='C' || nazwa=='J' || nazwa=='T'){
                        sw.rosliny++;
                    }
                    else{
                        sw.zwierzeta++;
                    }
                    sw.populacja++;
                    sw.organizmy[i][j]=nowy;
                    nowy.ZmienPolozenieX(i);
                    nowy.ZmienPolozenieY(j);
                }
            }
        }
    }
}