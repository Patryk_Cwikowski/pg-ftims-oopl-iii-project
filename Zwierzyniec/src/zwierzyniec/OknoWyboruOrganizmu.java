package zwierzyniec;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import organizm.Organizm;
import rosliny.Ciern;
import rosliny.Jagody;
import rosliny.Trawa;
import swiat.Swiat;
import zwierzeta.Antylopa;
import zwierzeta.Czlowiek;
import zwierzeta.Lew;
import zwierzeta.Owca;
import zwierzeta.Wilk;

public class OknoWyboruOrganizmu extends JPanel implements ActionListener{
    private JButton Lew;
    private JButton Wilk;
    private JButton Czlowiek;
    private JButton Owca;
    private JButton Antylopa;
    private JButton Trawa;
    private JButton Ciern;
    private JButton WilczeJagody;
    private JButton Cofnij;
    int x;
    int y;
    Swiat swiat;
    JLabel tekst;

    public OknoWyboruOrganizmu(Swiat sw, int xO, int yO, JLabel tekstO) {
        tekst=tekstO;
        swiat=sw;
        x=xO;
        y=yO;

        Lew = new JButton("Lew");
        Lew.addActionListener(this);
        Wilk = new JButton("Wilk");
        Wilk.addActionListener(this);
        Czlowiek = new JButton("Czlowiek");
        Czlowiek.addActionListener(this);
        Owca = new JButton("Owca");
        Owca.addActionListener(this);
        Antylopa = new JButton("Antylopa");
        Antylopa.addActionListener(this);
        Trawa = new JButton("Trawa");
        Trawa.addActionListener(this);
        Ciern = new JButton("Ciern");
        Ciern.addActionListener(this);
        WilczeJagody = new JButton("WilczeJagody");
        WilczeJagody.addActionListener(this);
        Cofnij = new JButton("Cofnij");
        Cofnij.addActionListener(this);

        add(Lew);
        add(Wilk);
        add(Czlowiek);
        add(Owca);
        add(Antylopa);
        add(Trawa);
        add(Ciern);
        add(WilczeJagody);
        add(Cofnij);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        Organizm nowy=null;
        int dodaj=1;
        if(source==Lew){
            nowy=new Lew(swiat);
        }
        else if(source==Wilk){
            nowy=new Wilk(swiat);
        }
        else if(source==Czlowiek){
            nowy=new Czlowiek(swiat);
        }
        else if(source==Owca){
            nowy=new Owca(swiat);
        }
        else if(source==Antylopa){
            nowy=new Antylopa(swiat);
        }
        else if(source==Trawa){
            nowy=new Trawa(swiat);
        }
        else if(source==Ciern){
            nowy=new Ciern(swiat);
        }
        else if(source==WilczeJagody){
            nowy=new Jagody(swiat);
        }
        else if(source==Cofnij){
            dodaj=0;
            if(swiat.organizmy[x][y]!=null){
                char nazwa=swiat.organizmy[x][y].ZwrocNazwe();
                if( nazwa=='C' || nazwa=='J' || nazwa=='T'){
                    swiat.rosliny--;
                }
                else{
                    swiat.zwierzeta--;
                }
                swiat.populacja--;
                swiat.organizmy[x][y]=null;
            }

        }
        if(dodaj==1){
            if(nowy.ZwrocNazwe()=='C' || nowy.ZwrocNazwe()=='J' || nowy.ZwrocNazwe()=='T'){
                swiat.rosliny++;
            }
            else{
                swiat.zwierzeta++;
            }
            nowy.ZmienPolozenieX(x);
            nowy.ZmienPolozenieY(y);
            swiat.organizmy[x][y]=nowy;
            swiat.populacja++;
        }
        tekst.setText("Populacja: "+swiat.populacja+"    Rosliny: "+swiat.rosliny+"   Zwierzeta: "+swiat.zwierzeta);
    }
}

class Okno extends JFrame{
    public Okno(Swiat sw, int x, int y,JLabel tekst) {
        super("Wybierz organizm");

        OknoWyboruOrganizmu okno= new OknoWyboruOrganizmu(sw,x,y,tekst);
        add(okno);
        okno.setBackground(new Color(76,139,183));
        setSize(380, 140);
        setLocation(802,70);
        setVisible(true);
    }
}