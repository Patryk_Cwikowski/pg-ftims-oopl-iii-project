package zwierzyniec;

import java.awt.EventQueue;
import swiat.Swiat;

public class Zwierzyniec {

    public static void main(String[] args) {
        Swiat sw=new Swiat();

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MyFrame(sw);
            }
        });
    }

}
