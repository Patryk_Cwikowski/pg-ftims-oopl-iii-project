package zwierzyniec;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import swiat.Swiat;

public class Legenda extends JPanel{
    public Legenda(Swiat sw){
        setSize(100,100);
        setLocation(10,10);
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.GRAY);
        g2d.fillRect(460, 40, 30,170);
        g2d.drawRect(460, 40, 30,170);
        g2d.setColor(Color.DARK_GRAY);
        g2d.fillRect(470, 50, 10,10);
        g2d.drawRect(470, 50, 10,10);
        g2d.setColor(Color.WHITE);
        g2d.fillRect(470, 70, 10,10);
        g2d.drawRect(470, 70, 10,10);
        g2d.setColor(new Color(255,135,20));
        g2d.fillRect(470, 90, 10,10);
        g2d.drawRect(470, 90, 10,10);
        g2d.setColor(Color.BLACK);
        g2d.fillRect(470, 110, 10,10);
        g2d.drawRect(470, 110, 10,10);
        g2d.setColor(Color.YELLOW);
        g2d.fillRect(470,130, 10,10);
        g2d.drawRect(470, 130, 10,10);
        g2d.setColor(new Color(124,252,0));
        g2d.fillRect(470, 150, 10,10);
        g2d.drawRect(470, 150, 10,10);
        g2d.setColor(new Color(150,75,0));
        g2d.fillRect(470, 170, 10,10);
        g2d.drawRect(470, 170, 10,10);
        g2d.setColor(new Color(102,0,102));
        g2d.fillRect(470, 190, 10,10);
        g2d.drawRect(470, 190, 10,10);
    }
}
