package zwierzyniec;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import swiat.Swiat;

public class ButtonPanel extends JPanel implements ActionListener{
    Swiat swiat;
    private JButton tura;
    private JButton zapisz;
    private JButton odczytaj;
    JLabel tekst;
    public ButtonPanel(Swiat sw, JLabel tekstO) {
        setSize(100,100);
        swiat=sw;
        tekst=tekstO;
        tura = new JButton("Tura");
        tura.addActionListener(this);
        zapisz = new JButton("Zapisz");
        zapisz.addActionListener(this);
        odczytaj = new JButton("Odczytaj");
        odczytaj.addActionListener(this);
        add(tura);
        add(zapisz);
        add(odczytaj);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source==tura){
            tekst.setText("Populacja: "+swiat.populacja+"    Rosliny: "+swiat.rosliny+"   Zwierzeta: "+swiat.zwierzeta);
            swiat.WykonajTure();
        }
        else if(source==zapisz){
            try {
                Plik zapis=new Plik();
                zapis.PlikZapisz(swiat.organizmy);
            }
            catch (FileNotFoundException ex) {
                JFrame wyjatek = new Wyjatek("zapis");
            }
        }
        else if(source==odczytaj){
            Plik odczyt=new Plik();
            try {
                odczyt.PlikOdczytaj(swiat);
                tekst.setText("Populacja: "+swiat.populacja+"    Rosliny: "+swiat.rosliny+"   Zwierzeta: "+swiat.zwierzeta);
            }
            catch (FileNotFoundException ex) {
                JFrame wyjatek = new Wyjatek("odczyt");
            }
        }
    }

}
