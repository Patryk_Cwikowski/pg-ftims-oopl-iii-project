package zwierzyniec;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import swiat.Swiat;

class Rysowanie extends JPanel implements MouseListener{
    Swiat swiat;
    JLabel tekst;
    public Rysowanie(Swiat sw, JLabel tekstO) {
        tekst=tekstO;
        swiat=sw;
        setSize(400, 400);
        addMouseListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.GRAY);
        g2d.fillRect(0, 0, 400, 400);
        g2d.drawRect(0, 0, 400,400);
        swiat.RysujSwiat(g2d);
        repaint();
    }
    private void UsunOrganizm(int x, int y, JLabel tekst){
        char nazwa=swiat.organizmy[x][y].ZwrocNazwe();
        if(nazwa=='T' || nazwa=='C' || nazwa=='J'){
            swiat.rosliny--;
        }
        else{
            swiat.zwierzeta--;
        }
        swiat.organizmy[x][y]=null;
        swiat.populacja--;
        tekst.setText("Populacja: "+swiat.populacja+"    Rosliny: "+swiat.rosliny+"   Zwierzeta: "+swiat.zwierzeta);
        repaint();
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        int x = e.getX()/20;
        int y = e.getY()/20;

        if(swiat.organizmy[x][y]!=null){
            UsunOrganizm(x,y,tekst);

        }
        else {
            JFrame okno=new Okno(swiat,x,y,tekst);

        }
    }
    @Override
    public void mousePressed(MouseEvent e) {}
    @Override
    public void mouseReleased(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseExited(MouseEvent e) {}

}

class MyFrame extends JFrame{
    public MyFrame(Swiat sw) {
        super("Zwierzyniec");
        JLabel tekst=new JLabel("Populacja: "+sw.populacja+"    Rosliny: "+sw.rosliny+"   Zwierzeta: "+sw.zwierzeta);
        JPanel panel = new Rysowanie(sw,tekst);
        JPanel legenda = new Legenda(sw);
        JPanel buttonPanel = new ButtonPanel(sw,tekst);
        JLabel wilk=new JLabel("Wilk");
        JLabel owca=new JLabel("Owca");
        JLabel lew=new JLabel("Lew");
        JLabel czlowiek=new JLabel("Czlowiek");
        JLabel antylopa=new JLabel("Antylopa");
        JLabel trawa=new JLabel("Trawa");
        JLabel ciern=new JLabel("Ciern");
        JLabel jagoda=new JLabel("Jagoda");

        add(buttonPanel);

        add(panel);
        add(wilk);
        add(owca);
        add(lew);
        add(czlowiek);
        add(antylopa);
        add(trawa);
        add(ciern);
        add(jagoda);
        add(tekst);
        add(legenda);

        pack();

        wilk.setSize(50,50);
        wilk.setLocation(500,28);
        owca.setSize(50,50);
        owca.setLocation(500,49);
        lew.setSize(50,50);
        lew.setLocation(500,69);
        czlowiek.setSize(70,50);
        czlowiek.setLocation(500,89);
        antylopa.setSize(70,50);
        antylopa.setLocation(500,109);
        trawa.setSize(50,50);
        trawa.setLocation(500,129);
        ciern.setSize(50,50);
        ciern.setLocation(500,149);
        jagoda.setSize(50,50);
        jagoda.setLocation(500,169);
        tekst.setSize(400,50);
        tekst.setLocation(80,460);
        buttonPanel.setLocation(470,250);
        panel.setLocation(40,40);
        setSize(600, 600);
        setLocation(200,70);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        repaint();
    }
}