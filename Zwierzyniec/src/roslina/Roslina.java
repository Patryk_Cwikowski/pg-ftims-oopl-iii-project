package roslina;

import java.util.Random;
import organizm.Organizm;
import rosliny.Ciern;
import rosliny.Jagody;
import rosliny.Trawa;
import swiat.Swiat;

public abstract class Roslina extends Organizm{

    @Override
    public void Akcja(Organizm tmp, char nazwa){
        Swiat sw=tmp.ZwrocSwiat();
        Organizm nowy=null;
        Random generator = new Random();
        int polozenieX=0;
        int polozenieY=0;
        int wyznacz=0;
        int polozenie=0;
        int liczba=generator.nextInt(40) + 1;
        // System.out.print(liczba+"\n");
        switch(liczba){
            case 1:
                polozenieX-=1;
                break;
            case 2:
                polozenieX=1;
                break;
            case 3:
                polozenieY-=1;
                break;
            case 4:
                polozenieY=1;
                break;
        }
        if(polozenieX!=0){
            if(polozenieX>=0 && polozenieX<20){
                polozenie=tmp.ZwrocPolozenieX()+polozenieX;
                if(polozenie>=20 || polozenie<0){
                    polozenie=tmp.ZwrocPolozenieX()-polozenieX;;
                }
                polozenieX=polozenie;
                polozenieY=tmp.ZwrocPolozenieY();
                wyznacz=1;
            }
        }
        else if(polozenieY!=0){
            if(polozenieX>=0 && polozenieX<20){
                polozenie=tmp.ZwrocPolozenieY()+polozenieY;
                if(polozenie>=20 || polozenie<0){
                    polozenie=tmp.ZwrocPolozenieY()-polozenieY;;
                }
                polozenieY=polozenie;
                polozenieX=tmp.ZwrocPolozenieX();
                wyznacz=1;
            }
        }
        if(wyznacz==1 && sw.organizmy[polozenieX][polozenieY]==null){
            if(nazwa=='T'){
                nowy=new Trawa(sw);
            }
            else if(nazwa=='J'){
                nowy=new Jagody(sw);
            }
            else if(nazwa=='C'){
                nowy=new Ciern(sw);
            }
            nowy.ZmienPolozenieX(polozenieX);
            nowy.ZmienPolozenieY(polozenieY);
            sw.organizmy[polozenieX][polozenieY]=nowy;
            sw.rosliny++;
            sw.populacja++;
        }
    }
    @Override
    public void Kolizja(Organizm tmp1, Organizm tmp2){

    }
}
