package zwierze;

import java.util.Random;
import organizm.Organizm;
import swiat.Swiat;
import zwierzeta.Antylopa;
import zwierzeta.Czlowiek;
import zwierzeta.Lew;
import zwierzeta.Wilk;

public abstract class Zwierze extends Organizm{

    protected void DodajZwierze(Organizm nowy, Organizm tmp2,int polozenieX, int polozenieY){
        nowy.ZmienPolozenieX(polozenieX);
        nowy.ZmienPolozenieY(polozenieY);
        nowy.ZmienRodzenie(rodzenie);
        tmp2.ZmienRuch(0);
        sw.organizmy[polozenieX][polozenieY]=nowy;
        sw.populacja++;
        sw.zwierzeta++;
    }
    protected void UstalPolozenie(int polozenieX1, int polozenieY1, int polozenieX, int polozenieY){
        if(polozenieX<0 || polozenieX>19 || polozenieY<0 || polozenieY>19 || sw.organizmy[polozenieX][polozenieY]!=null &&  (sw.organizmy[polozenieX][polozenieY].ZwrocNazwe()!='T' )){
            polozenieX-=2;
        }
        if(polozenieX<0 || polozenieX>19 || polozenieY<0 || polozenieY>19 || sw.organizmy[polozenieX][polozenieY]!=null  && ( sw.organizmy[polozenieX][polozenieY].ZwrocNazwe()!='T' )){
            polozenieX=polozenieX1;
            polozenieY=polozenieY1-1;
        }
        if(polozenieX<0 || polozenieX>19 || polozenieY<0 || polozenieY>19 || sw.organizmy[polozenieX][polozenieY]!=null  && ( sw.organizmy[polozenieX][polozenieY].ZwrocNazwe()!='T' )){
            polozenieY=polozenieY1+2;
            polozenieX=polozenieX1;
        }
    }
    @Override
    public void Akcja(Organizm tmp, char nazwa){
        Swiat sw=tmp.ZwrocSwiat();
        Random generator = new Random();
        int wyznacz=1;
        while(wyznacz==1){
            int liczba=generator.nextInt(4) + 1;
            int polozenie=0;
            int polozenieX=0;
            int polozenieY=0;
            switch(liczba){
                case 1:
                    polozenieX-=1;
                    break;
                case 2:
                    polozenieX=1;
                    break;
                case 3:
                    polozenieY-=1;
                    break;
                case 4:
                    polozenieY=1;
                    break;
            }
            if(polozenieX!=0){
                polozenie=tmp.ZwrocPolozenieX();
                polozenie=polozenie+polozenieX;
                if(polozenie>=20 || polozenie<0){
                    polozenie=polozenie-polozenieX-polozenieX;
                }
                if(sw.organizmy[polozenie][tmp.ZwrocPolozenieY()]!=null){
                    tmp.Kolizja(tmp,sw.organizmy[polozenie][tmp.ZwrocPolozenieY()]);
                    wyznacz=0;

                }
                else{
                    int x=tmp.ZwrocPolozenieX();
                    int y=tmp.ZwrocPolozenieY();
                    tmp.ZmienPolozenieX(polozenie);
                    sw.organizmy[x][y]=null;
                    sw.organizmy[polozenie][y]=tmp;
                    wyznacz=0;
                }
            }
            else if(polozenieY!=0){
                polozenie=tmp.ZwrocPolozenieY();
                polozenie=polozenie+polozenieY;
                if(polozenie>=20 || polozenie<0){
                    polozenie=polozenie-polozenieY-polozenieY;
                }
                if(sw.organizmy[tmp.ZwrocPolozenieX()][polozenie]!=null){
                    tmp.Kolizja(tmp, sw.organizmy[tmp.ZwrocPolozenieX()][polozenie]);
                    wyznacz=0;

                }
                else{
                    int x=tmp.ZwrocPolozenieX();
                    int y=tmp.ZwrocPolozenieY();
                    tmp.ZmienPolozenieY(polozenie);
                    sw.organizmy[x][y]=null;
                    sw.organizmy[x][polozenie]=tmp;
                    wyznacz=0;
                }
            }
        }
    }
    @Override
    public void Kolizja(Organizm tmp1, Organizm tmp2){
        Swiat sw=tmp1.ZwrocSwiat();
        int polozenieX1=tmp1.ZwrocPolozenieX();
        int polozenieY1=tmp1.ZwrocPolozenieY();
        int polozenieX2=tmp2.ZwrocPolozenieX();
        int polozenieY2=tmp2.ZwrocPolozenieY();
        if(tmp2.ZwrocNazwe()=='J'){
            if(tmp1.ZwrocNazwe()=='P'){
                sw.organizmy[polozenieX2][polozenieY2]=sw.organizmy[polozenieX1][polozenieY1];
                sw.organizmy[polozenieX1][polozenieY1]=null;
                tmp1.ZmienPolozenieX(polozenieX2);
                tmp1.ZmienPolozenieY(polozenieY2);
                sw.rosliny--;
                sw.populacja--;
            }
            else{
                sw.organizmy[polozenieX1][polozenieY1]=null;
                sw.organizmy[polozenieX2][polozenieY2]=null;
                sw.rosliny--;
                sw.zwierzeta--;
                sw.populacja-=2;
            }

        }
        else if(tmp1.ZwrocSile()>tmp2.ZwrocSile()){
            if(tmp2.ZwrocNazwe()=='C' || tmp2.ZwrocNazwe()=='T' || tmp2.ZwrocNazwe()=='J'){
                sw.rosliny--;
            }
            else{
                sw.zwierzeta--;
            }
            sw.organizmy[polozenieX2][polozenieY2]=sw.organizmy[polozenieX1][polozenieY1];
            sw.organizmy[polozenieX1][polozenieY1]=null;
            tmp1.ZmienPolozenieX(polozenieX2);
            tmp1.ZmienPolozenieY(polozenieY2);
            sw.populacja--;
        }
        else if(tmp1.ZwrocSile()<5 && tmp2.ZwrocNazwe()=='L'){

        }
        else if(tmp1.ZwrocNazwe()=='A' && tmp1.ZwrocSile()<tmp2.ZwrocSile()){
            Random generator = new Random();
            int liczba=generator.nextInt(2) + 1;
            switch(liczba){
                case 1:
                    sw.organizmy[polozenieX1][polozenieY2]=null;
                    sw.populacja--;
                    sw.zwierzeta--;
                    break;
                case 2:
                    tmp1.Akcja(tmp1,tmp1.ZwrocNazwe());
                    break;
            }
        }
        else if(tmp1.ZwrocNazwe()=='P' && tmp1.ZwrocSile()<tmp2.ZwrocSile()){
            tmp1.Akcja(tmp1,tmp1.ZwrocNazwe());
        }
        else if(tmp1.ZwrocSile()<tmp2.ZwrocSile()){
            if(tmp1.ZwrocNazwe()=='T' || tmp1.ZwrocNazwe()=='C' || tmp1.ZwrocNazwe()=='J'){
                sw.rosliny--;
            }
            else{
                sw.zwierzeta--;
            }
            sw.organizmy[polozenieX1][polozenieY1]=null;
            sw.populacja--;
        }
        else if(tmp1.ZwrocSile()==tmp2.ZwrocSile()){
            tmp1.Akcja(tmp1, tmp1.ZwrocNazwe());
        }
    }
}
