package organizm;

import swiat.Swiat;

abstract public class Organizm {
    protected char nazwa;
    protected int sila;
    protected int inicjatywa;
    protected int polozenie_x;
    protected int polozenie_y;
    protected int ruch;
    protected int rodzenie;
    protected int wiek;
    protected Swiat sw;

    public void ZmienRuch(int ruchO){
        ruch=ruchO;
    }
    public void ZmienPolozenieX(int polozenie){
        polozenie_x=polozenie;
    }
    public void ZmienPolozenieY(int polozenie){
        polozenie_y=polozenie;
    }
    public void ZmienRodzenie(int rodzenieO){
        rodzenie=rodzenieO;
    }
    public void ZmienWiek(int wiekO){
        wiek=wiekO;
    }
    public int ZwrocRuch(){
        return ruch;
    }
    public Swiat ZwrocSwiat(){
        return sw;
    }
    public int ZwrocPolozenieX(){
        return polozenie_x;
    }
    public int ZwrocPolozenieY(){
        return polozenie_y;
    }
    public char ZwrocNazwe(){
        return nazwa;
    }
    public int ZwrocInicjatywe(){
        return inicjatywa;
    }
    public int ZwrocRodzenie(){
        return rodzenie;
    }
    public int ZwrocSile(){
        return sila;
    }
    public int ZwrocWiek(){
        return wiek;
    }
    abstract public void Akcja(Organizm tmp, char nazwa);
    abstract public void Kolizja(Organizm tmp1, Organizm tmp2);
}
