========ZWIERZYNIEC=======
It is a program that simulates the interaction between animals, humans and plants.
Objects depending on their type, respectively, may reproduce, die, kill each other or eat each other.

=====CONTROLS=====
"Tura" button - It executes the next move by all objects on the map.
"Zapisz" button - Write to file swiat.txt current state map.
"Odczytaj" button - Loads of an existing file previously saved state swiat.txt maps.
Mouse button - Opens selection of objects that can be created in the selected cell.